


// displayed sum

function sumOfTwoNumbers(num1,num2){
    
    let sum = num1 + num2;
	console.log("Displayed sum of "+ num1 + " and " + num2);
	console.log(sum);
}

sumOfTwoNumbers(5,15);

function differenceOfTwoNumbers(num1,num2){
    
    let difference = num1 - num2;
	console.log("Displayed difference of "+ num1 + " and " + num2);
	console.log(difference);
}

differenceOfTwoNumbers(20,5);

function productOfTwoNumbers(num1,num2){
	let product = num1 * num2;
	return product;
}

let multiply = productOfTwoNumbers(50,10);
console.log('the product of 50 and 10:');
console.log(multiply);

function quotientOfTwoNumbers(num1,num2){
	let quotient = num1 / num2;
	return quotient;
}

let quotient = quotientOfTwoNumbers(50,10);
console.log('the quotient of 50 and 10:');
console.log(quotient);

function areaOfCircle(radius){
	let circleArea = 3.14*radius**2
	return circleArea;
};

let circleArea = areaOfCircle(15);
console.log('the result of getting the are of a circle with 15 radius: ')
console.log(circleArea);

function averageOfFourNumbers(num1,num2,num3,num4){
	let averageVar = (num1 + num2 + num3 + num4) / 4;
	return averageVar;
}

let averageVar = averageOfFourNumbers(20,40,60,80);
console.log('The average of 20,40,60 and 80: ');
console.log(averageVar);

function passingScorePercentage(score,totalscore){
	let yourScorePercentage = (score/totalscore) * 100;
	let isPassed = yourScorePercentage >= 75;
	return isPassed;
}

let isPassed = passingScorePercentage(38,50);
console.log('is 38/50 a passing score: ');
console.log(isPassed);
